'''
Created on 28 окт. 2020 г.

@author: AGorlov
'''

from os import listdir
from os.path import isfile, join

class DictionaryManger(object):
    '''
    This class loads and manages set of dictionaries
    '''
    
    @property
    def Dictionary_Content(self):
        return self.dic_content_list
    
    def __init__(self, lang_name):
        '''
        Constructor
        '''
        self.lang_name = lang_name
        self.dic_content_list = dict()
    
    def Dictionary_Loader(self, folder_name) -> bool:
        dic_list = [f for f in listdir(folder_name) if isfile(join(folder_name, f))]
        if len(dic_list) == 0:
            print("The dictionary not found. May be given language is not support")
            return False
        dic_list = list(map(lambda name : join(folder_name, name), dic_list))
        for dic in dic_list:
            with open(dic, encoding='utf-8', mode='r') as fh:
                dic_content = fh.readlines()
                dic_content = list(map(lambda word : word.rstrip("\n"), dic_content))
                self.dic_content_list[dic_content[0][0]] = dic_content
        return True
                
    def Is_Word_Good(self, word : str) -> bool:
        key = word[0]
        try :
            word_list = self.dic_content_list[key]
            if word in word_list:
                return True
            else:
                return False
        except KeyError as error:
            print(f"Hmm ... Internall error. Could not find corresponded dictionary for {word}")
            return False
    
        
    
            
        