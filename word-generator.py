# -*- coding: utf-8 -*-
import itertools
import re
import argparse
import sys
from datetime import datetime
import lib.Dictionary_Manager

#абвгдеёжзиклимнопрстуфчцчшщэюя

Rejected_List = list()
Dictionary_Manager = lib.Dictionary_Manager.DictionaryManger("ru")


def Read_Data(args):
    with open(args.file_name, encoding = 'utf-8', mode = 'r') as my_file:
        text= my_file.read()
    return text


def Check_Good_Words(word : str):
    global Rejected_List
    global Dictionary_Manager
    vowels = u'аеёийоуэюя'
    consonant = u'бвгджзклмнпрстфчцчшщ'
    if word[0] == 'ь':
        return False
    elif word[0] == 'ъ':
        return False
    elif all((symbol in vowels) for symbol in word ):
        if word != 'аи':
            return False
        else:
            return True
    elif all((symbol in consonant) for symbol in word ):
        return False
    elif not Dictionary_Manager.Is_Word_Good(word):
        Rejected_List.append(word)
        return False
    else:
        return True


def Run_Word_Generator(word : str, minimal_word_size : int):
    global Common_Duration
    print("Generating words")
    word_tuple_list = list()
    word_list = list()

    for number in range(minimal_word_size, len(word) + 1):
        start_time = datetime.now()
        print("Word size : " + str(number) + " ", end='')
        word_tuple_list = set(itertools.permutations(word, number))
        word_list = list(map(u"".join, word_tuple_list))
        word_list = list(filter(Check_Good_Words, word_list))
        Save_Result(word_list, number)
        end_time = datetime.now()
        duration = end_time - start_time
        print('Duration: {}'.format(end_time - start_time))

def Save_Rejected_Word():
    global Rejected_List
    print("Print rejected list")
    with open ("rejected.txt", encoding='utf-8', mode='w') as fh:
        for word in Rejected_List:
            print(word, file=fh)

def Save_Result(word_list : list, size : int):
    file_name = "word-of-" + str(size)+".txt"
    with open(file_name, encoding = "utf-8", mode="w") as my_file:
        word_list = sorted(word_list)
        for word in word_list:
            print(u''.join(word), file=my_file)

def Command_Line_Parser() :
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group(required = True)
    group.add_argument('-f', '--file', help='file name where word for generating subword is located', dest='file_name')
    group.add_argument('-w', '--word', help='source word', dest='word')
    parser.add_argument('-m', '--minimal', type=int, default=3, help='minimal size of generated word', dest='minimal_word_size')
    parser.add_argument('-l', '--language', default = "ru", help='target language for word genereating.Default RU', dest='lang_name')
    parser.add_argument('-v', '--version',
                        action='version',
                        version='Current Version of ' + sys.argv[0] + ' : 0.0.6',
                        help='print current version')
    args = parser.parse_args()
    return args


def main():
    start_time = datetime.now()
    global Dictionary_Manager
    args = Command_Line_Parser()

    rc = Dictionary_Manager.Dictionary_Loader("./dictionaries/" + args.lang_name)
    if not rc:
        return
    if args.word :
        word = args.word
    else:
        word = Read_Data(args)

    Run_Word_Generator(args.word, args.minimal_word_size)
    Save_Rejected_Word()
    end_time = datetime.now()
    duration = end_time - start_time
    print('Duration: {}'.format(end_time - start_time))

if __name__ == '__main__':
    main()

