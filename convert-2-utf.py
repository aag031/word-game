import codecs

f = codecs.open("zdf-win.txt", 'r', 'cp1251')
u = f.read()   # now the contents have been transformed to a Unicode string
out = codecs.open("zdf-win-utf-8.txt", 'w', 'utf-8')
out.write(u)   # and now the contents have been output as UTF-8