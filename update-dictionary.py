# -*- coding: utf-8 -*-
import sys

file_name = sys.argv[1]
print(file_name)
with open(file_name , encoding='utf-8', mode='r') as hf:
    content = hf.readlines()
result = list()
print ("Old Size " + str(len(content)) + " ", end ="")
for line in content:
    line = line.rstrip('\n')
    if line.endswith("ый"):
        continue
    elif line.endswith("ий"):
        continue
    elif line.endswith("ой"):
        continue
    elif line.endswith("ая"):
        continue
    elif line.endswith("ться"):
        continue
    elif line.endswith("сть"):
        result.append(line)
    elif line.endswith("ть"):
        continue
    else:
        result.append(line)
print ("New Size " + str(len(result)))
with open(file_name + ".1" , encoding='utf-8', mode='w') as hf2:
    for line2 in result:
        print(line2, file=hf2)